#include <ThingSpeak.h>
#include <Adafruit_DHT.h>

#define DHTPIN D6
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

TCPClient client;
unsigned long channelNumber = 1369798;
const char * writeAPIKey = "L991S0CF3ZVBVVBE";


void setup() {
    
 ThingSpeak.begin(client);
 Serial.begin(9600); 
 dht.begin();
 Particle.subscribe("Paul_Clarke", l, ALL_DEVICES);
 
}

void loop() {

    // retrieve humidity and temp and check if the reading is valid, print if invalid
    float h = dht.getHumidity();
    float t = dht.getTempCelcius();
    if (isnan(h) || isnan(t)) {
        Serial.println("Sensor reading failed!");
        return;
    }
    
    // print details
    Particle.publish("Paul_Clarke", String (t), PUBLIC);
    Particle.publish("Paul_Clarke", String (h), PUBLIC);

  // publish to ThingSpeak
  ThingSpeak.setField(1,t);
  ThingSpeak.setField(2,h);

  ThingSpeak.writeFields(channelNumber, writeAPIKey);  

  // delay by 5 seconds, ThingSpeak only accepts data every 5 seconds
  delay(5000);

}

    void l(const char *event, const char *data) {
       
    }